import express from 'express';
import expressJwt from 'express-jwt';
import httpStatus from 'http-status';
import mysql from 'mysql';
import config from '../../config/env';
import APIError from '../helpers/APIError';

const connection = mysql.createPool(config.db);

const router = express.Router(); // eslint-disable-line new-cap

router.use(expressJwt({
    secret: config.jwtSecret,
}));

router.route('/')
    .get((req, res, next) => {
        // let assessId = 24133;
        let sql = `SELECT
        student.id,
        case student.title
            when 1 then 'ด.ช.'
            when 2 then 'ด.ญ.'
            when 3 then 'นาย'
            when 4 then 'น.ส.'
            when 5 then 'นาง'
        end as title,
        student.firstname,
        student.lastname,
        academy_class.name AS class,
        academy_class.short_name AS class_shortname,
        academy_classroom.room_id AS room,
        semester.name AS semaster,
        assess.assess_id
        FROM `+config.dbAssess+`.assess
        INNER JOIN `+config.dbAssess+`.assess_user
        ON assess_user.assess_id = assess.assess_id

        INNER JOIN `+config.dbSchool+`.student
        ON student.id = assess.student_id
        INNER JOIN `+config.dbSchool+`.student_class
        ON student_class.id = assess.student_class_id
        INNER JOIN `+config.dbSchool+`.semester
        ON semester.id = student_class.semester_id
        INNER JOIN `+config.dbSchool+`.academy_classroom
        ON academy_classroom.id = student_class.classroom_id
        INNER JOIN `+config.dbSchool+`.academy_class
        ON academy_class.id = academy_classroom.class_id
        WHERE assess.assess_id = 1
        `;
        connection.query(sql, function(err, data) {
            if (err) {
                console.log(err.error);
                const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                return next(err);
            } else if (data.length === 0){
                res.send(sql);
            } else {
                res.json(data);
            }
        });
    });

export default router;
