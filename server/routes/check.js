import express from 'express';
import mysql from 'mysql';
import async from 'async';
import config from '../../config/env';
let configAssess = config.db;
configAssess.database = config.dbAssess;
let configSchool = config.db;
configSchool.database = config.dbSchool;
const connectionAssess = mysql.createPool(configAssess);
const connectionSchool = mysql.createPool(configSchool);
const router = express.Router(); // eslint-disable-line new-cap

router.route('/').get((req, res) => {
    let data = [1,2];
    async.map(data, function(row,callback){
        if (row == 1) {
            connectionAssess.getConnection(function(err) {
                if(err) {
                    console.log('error when connecting to db:', err);
                    callback(err,{db1:'error when connecting to db:'+ err});
                } else {
                    callback(err,{db1:'ok'});
                }
            });
        } else {
            connectionSchool.getConnection(function(err) {
                if(err) {
                    console.log('error when connecting to db:', err);
                    callback(err,{db2:'error when connecting to db:'+ err});
                } else {
                    callback(err,{db2:'ok'});
                }
            });
        }
    }, function(err, results) {
        let responseData = results;
        responseData.push({api:'ok'});
        res.json(responseData);
    });
});

router.route('/db1').get((req, res) => {
    connectionAssess.getConnection(function(err) {
        if(err) {
            console.log('error when connecting to db:', err);
            res.send('error when connecting to db:'+ err);
        } else {
            res.send('connect OK');
        }
    });
});

router.route('/db2').get((req, res) => {
    connectionSchool.getConnection(function(err) {
        if(err) {
            console.log('error when connecting to db:', err);
            res.send('error when connecting to db:'+ err);
        } else {
            res.send('connect OK');
        }
    });
});

export default router;
