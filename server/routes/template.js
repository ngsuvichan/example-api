import express from 'express';
import expressJwt from 'express-jwt';
import httpStatus from 'http-status';
import mysql from 'mysql';
import validate from 'express-validation';
import async from 'async';
import paramValidation from '../../config/param-validation';
import config from '../../config/env';
import APIError from '../helpers/APIError';

const connection = mysql.createPool(config.db);
const router = express.Router(); // eslint-disable-line new-cap

router.use(expressJwt({
    secret: config.jwtSecret,
}));

router.route('/')
    .get((req, res, next) => {
        if (req.query.role) {
            return next();
        }
        let sql = `SELECT * FROM ` + config.dbAssess + `.assess_template`
        connection.query(sql, function(err, data) {
            if (err) {
                const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                return next(err);
            } else {
                res.json(data);
            }
        });
    }).get(validate(paramValidation.checkTemplateFilter), (req, res, next) => {
        //filter role
        let role = '%' + req.query.role + '%';
        let sql = `SELECT * FROM ` + config.dbAssess + `.assess_template WHERE template_for LIKE ?`
        connection.query(sql, [role], function(err, data) {
            if (err) {
                const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                return next(err);
            } else {
                res.json(data);
            }
        });
    })
    //http://localhost:5000/api/template/1
router.route('/:id')
    .get((req, res, next) => {
        let id = req.params.id;
        let sql = `SELECT * FROM ` + config.dbAssess + `.assess_template WHERE template_id = ?`
        connection.query(sql, [id], function(err, data) {
            if (err) {
                const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                return next(err);
            } else {
                res.json(data);
            }
        });
    });

router.route('/:id/send-list')
    .get(validate(paramValidation.checkSendList), (req, res, next) => {
        let templateId = req.params.id;
        let role = req.query.role;
        let semesterId = req.query.semester_id;
        let schoolId = req.user.school_id;
        let sql = `SELECT
                academy_classroom.id AS classroom_id,
                academy_classroom.class_id,
                academy_class.name AS class_name,
                academy_class.short_name AS class_shortname,
                academy_classroom.room_id AS room,
                student_class.is_notsend,
                student_class.is_send,
                student_class.is_notsend+student_class.is_send AS total
            FROM (
                SELECT
                    SUM(case when assess.assess_id IS NULL then 1 else 0 end) AS is_notsend,
                    SUM(case when assess.assess_id IS NULL then 0 else 1 end) AS is_send
                    ,student_class.classroom_id
                FROM ` + config.dbSchool + `.semester
                INNER JOIN ` + config.dbSchool + `.student_class
                ON semester.id = student_class.semester_id
                INNER JOIN ` + config.dbSchool + `.student
                ON student_class.student_id = student.id
                LEFT JOIN (
                    SELECT *
                    FROM ` + config.dbAssess + `.assess
                    WHERE assess.template_id = ?
                    AND assess.assess_for = ?
                    AND assess.delete_at IS NULL
                ) AS assess
                ON (student.id = assess.student_id AND student_class.id = assess.student_class_id)
                WHERE semester.school_id = ?
                AND semester.id = ?
                AND student.status = 10
                GROUP BY student_class.classroom_id
            ) student_class
            INNER JOIN ` + config.dbSchool + `.academy_classroom
            ON student_class.classroom_id = academy_classroom.id
            INNER JOIN ` + config.dbSchool + `.academy_class
            ON academy_classroom.class_id = academy_class.id
            ORDER BY academy_class.name,CAST(academy_classroom.room_id AS DECIMAL(5,0))`
        connection.query(sql, [templateId, role, schoolId, semesterId], function(err, data) {
            if (err) {
                const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                return next(err);
            } else {
                res.json(data);
            }
        });
    });
router.route('/:id/send-list/:classroom_id')
    .get(validate(paramValidation.checkSendList), (req, res, next) => {
        let templateId = req.params.id;
        let role = req.query.role;
        let semesterId = req.query.semester_id;
        let schoolId = req.user.school_id;
        let classroomId = req.params.classroom_id;
        let sql = `SELECT
                academy_classroom.id AS classroom_id,
                academy_classroom.class_id,
                academy_class.name AS class_name,
                academy_class.short_name AS class_shortname,
                academy_classroom.room_id AS room,
                student_class.is_notsend,
                student_class.is_send,
                student_class.is_notsend+student_class.is_send AS total
            FROM (
                SELECT
                    SUM(case when assess.assess_id IS NULL then 1 else 0 end) AS is_notsend,
                    SUM(case when assess.assess_id IS NULL then 0 else 1 end) AS is_send
                    ,student_class.classroom_id
                FROM ` + config.dbSchool + `.semester
                INNER JOIN ` + config.dbSchool + `.student_class
                ON semester.id = student_class.semester_id
                INNER JOIN ` + config.dbSchool + `.student
                ON student_class.student_id = student.id
                LEFT JOIN (
                    SELECT *
                    FROM ` + config.dbAssess + `.assess
                    WHERE assess.template_id = ?
                    AND assess.assess_for = ?
                    AND assess.delete_at IS NULL
                ) AS assess
                ON (student.id = assess.student_id AND student_class.id = assess.student_class_id)
                WHERE semester.school_id = ?
                AND semester.id = ?
                AND student.status = 10
                AND student_class.classroom_id = ?
                GROUP BY student_class.classroom_id
            ) student_class
            INNER JOIN ` + config.dbSchool + `.academy_classroom
            ON student_class.classroom_id = academy_classroom.id
            INNER JOIN ` + config.dbSchool + `.academy_class
            ON academy_classroom.class_id = academy_class.id
            ORDER BY academy_class.name,CAST(academy_classroom.room_id AS DECIMAL(5,0))`
        connection.query(sql, [templateId, role, schoolId, semesterId, classroomId], function(err, data) {
            if (err) {
                const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                return next(err);
            } else if (data.length === 0) {
                res.json(JSON.stringify({}));
            } else {
                res.json(data);
            }
        });
    });

router.route('/:id/send/:classroom_id')
    .post(validate(paramValidation.checkSendAssess), (req, res, next) => {
        let schoolId = req.user.school_id;
        let semesterId = req.body.semester_id;
        let role = req.body.role;
        let classroomId = req.params.classroom_id;
        let templateId = req.params.id;
        let sql = `SELECT
                    student.id AS student_id,
                    student.id AS user_id,
                    student_class.id AS student_class_id
                FROM schoolcare.semester
                INNER JOIN schoolcare.student_class
                ON semester.id = student_class.semester_id
                INNER JOIN schoolcare.student
                ON student_class.student_id = student.id
                LEFT JOIN (
                    SELECT *
                    FROM schoolassess.assess
                    WHERE assess.template_id = ?
                    AND assess.assess_for = ?
                ) AS assess
                ON (student.id = assess.student_id AND student_class.id = assess.student_class_id)
                WHERE assess.delete_at IS NULL
                AND semester.school_id = ?
                AND semester.id = ?
                AND student.status = 10
                AND student_class.classroom_id = ?
                AND assess.assess_id IS NULL`;

        connection.query(sql, [templateId, role, schoolId, semesterId, classroomId], function(err, data) {
            if (err) {
                const err = new APIError('Database Error 1', httpStatus.INTERNAL_SERVER_ERROR, true);
                return next(err);
            } else if (data.length === 0) {
                res.send("data is null");
            } else {
                var countSuccess = 0;
                async.map(data, function(row, callback) {
                    let sql = `INSERT INTO schoolassess.assess (student_id, student_class_id, template_id, assess_for) VALUES (?,?,?,?)`;
                    connection.query(sql, [row.student_id, row.student_class_id, templateId, role], function(err, dataInsert) {
                        if (err) {
                            const err = new APIError('Database Error 2', httpStatus.INTERNAL_SERVER_ERROR, true);
                            return next(err);
                        } else {
                            callback(err, dataInsert.affectedRows);
                        }
                    });
                }, function(err, results) {
                    results.map((item) => {
                        console.log("result:", item, ":", typeof item);
                        countSuccess += item;
                    })
                    let sql = `SELECT
                            academy_classroom.id AS classroom_id,
                            academy_classroom.class_id,
                            academy_class.name AS class_name,
                            academy_class.short_name AS class_shortname,
                            academy_classroom.room_id AS room,
                            student_class.is_notsend,
                            student_class.is_send,
                            student_class.is_notsend+student_class.is_send AS total
                        FROM (
                            SELECT
                                SUM(case when assess.assess_id IS NULL then 1 else 0 end) AS is_notsend,
                                SUM(case when assess.assess_id IS NULL then 0 else 1 end) AS is_send
                                ,student_class.classroom_id
                            FROM ` + config.dbSchool + `.semester
                            INNER JOIN ` + config.dbSchool + `.student_class
                            ON semester.id = student_class.semester_id
                            INNER JOIN ` + config.dbSchool + `.student
                            ON student_class.student_id = student.id
                            LEFT JOIN (
                                SELECT *
                                FROM ` + config.dbAssess + `.assess
                                WHERE assess.template_id = ?
                                AND assess.assess_for = ?
                                AND assess.delete_at IS NULL
                            ) AS assess
                            ON (student.id = assess.student_id AND student_class.id = assess.student_class_id)
                            WHERE semester.school_id = ?
                            AND semester.id = ?
                            AND student.status = 10
                            AND student_class.classroom_id = ?
                            GROUP BY student_class.classroom_id
                        ) student_class
                        INNER JOIN ` + config.dbSchool + `.academy_classroom
                        ON student_class.classroom_id = academy_classroom.id
                        INNER JOIN ` + config.dbSchool + `.academy_class
                        ON academy_classroom.class_id = academy_class.id
                        ORDER BY academy_class.name,CAST(academy_classroom.room_id AS DECIMAL(5,0))`
                    connection.query(sql, [templateId, role, schoolId, semesterId, classroomId], function(err, data) {
                        if (err) {
                            const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                            return next(err);
                        } else if (data.length === 0) {
                            res.json(JSON.stringify({}));
                        } else {
                            let responseData = {
                                data: data[0]
                            };
                            responseData.amount = countSuccess.toString();
                            responseData.message = 'Send ' + countSuccess.toString() + ' person success';
                            res.json(responseData);
                        }
                    });
                });
            }
        });
    }).delete(validate(paramValidation.checkSendAssess), (req, res, next) => {
        let schoolId = req.user.school_id;
        let semesterId = req.body.semester_id;
        let role = req.body.role;
        let classroomId = req.params.classroom_id;
        let templateId = req.params.id;
        let sql = `DELETE FROM schoolassess.assess
        WHERE assess.assess_id in (
        SELECT assess_id FROM schoolcare.semester
        INNER JOIN schoolcare.student_class ON semester.id = student_class.semester_id
        INNER JOIN schoolcare.student ON student_class.student_id = student.id
        INNER JOIN( SELECT * FROM schoolassess.assess WHERE assess.template_id = ?
        AND assess.assess_for = ?
        ) AS assess ON( student.id = assess.student_id AND student_class.id = assess.student_class_id
        ) WHERE assess.delete_at IS NULL
        AND semester.school_id = ?
        AND semester.id = ?
        AND student.status = 10
        AND student_class.classroom_id = ?
	)`
        connection.query(sql, [templateId, role, schoolId, semesterId, classroomId], function(err, data) {
            if (err) {
                const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                return next(err);
            } else {
                let responseData = {};
                responseData.amount = data.affectedRows;
                responseData.message = 'Detete ' + data.affectedRows + ' Student Success';
                res.json(responseData);
            }
        });
    });
export default router;
