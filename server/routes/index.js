import express from 'express';
import checkRoutes from './check';
import authRoutes from './auth';
import roleRoutes from './role';
import assessRoutes from './assess';
import userRoutes from './user';
import queryRoutes from './query';
import templateRoutes from './template';

const router = express.Router(); // eslint-disable-line new-cap

// mount auth routes at /auth
router.use('/check', checkRoutes);
router.use('/auth', authRoutes);
router.use('/role', roleRoutes);
router.use('/assess', assessRoutes);
router.use('/user', userRoutes);
router.use('/template', templateRoutes);
router.use('/query', queryRoutes);

export default router;
