import express from 'express';
import expressJwt from 'express-jwt';
import httpStatus from 'http-status';
import mysql from 'mysql';
import config from '../../config/env';
import APIError from '../helpers/APIError';

const connection = mysql.createPool(config.db);

const router = express.Router(); // eslint-disable-line new-cap

router.use(expressJwt({
    secret: config.jwtSecret,
}));

router.route('/semester')
    .get((req, res, next) => {
        let schoolId = req.user.school_id;
        let sql = `SELECT school_id,
        semester.id AS semester_id,
        start_date,end_date,
        semester.name AS semester_name,
        academicyear.name AS year
        FROM `+config.dbSchool+`.semester
        INNER JOIN `+config.dbSchool+`.academicyear
        ON semester.academic_id = academicyear.id
        WHERE semester.school_id = ?
        ORDER BY semester.start_date desc`;
        connection.query(sql, [schoolId], function(err, data) {
            if (err) {
                const err = new APIError('Database Error 2', httpStatus.INTERNAL_SERVER_ERROR, true);
                return next(err);
            } else {
                res.json(data);
            }
        });
    });

export default router;
