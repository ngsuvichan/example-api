import express from 'express';
import validate from 'express-validation';
import expressJwt from 'express-jwt';
import paramValidation from '../../config/param-validation';
import config from '../../config/env';

const router = express.Router();

router.use(expressJwt({
    secret: config.jwtSecret,
}));

const roleList = {
    'student':{
        'page':[
            {'page_id':101,'page_link':'/assess-list','page_text':'รายการประเมิน','page_icon':'list layout'}
        ],
        'user':{
            'type':'student'
        }
    },
    'parent':{
        'page':[
            {'page_id':101,'page_link':'/assess-list','page_text':'รายการประเมิน','page_icon':'list layout'},
            {'page_id':201,'page_link':'/student-list','page_text':'นักเรียน','page_icon':'users'}
        ],
        'user':{
            'type':'parent'
        }
    },
    'teacher':{
        'page':[
            {'page_id':102,'page_link':'/assess-class','page_text':'รายการประเมิน','page_icon':'list layout'},
            {'page_id':103,'page_link':'/assess-result','page_text':'ผลการประเมิน','page_icon':'bar chart'}
        ],
        'user':{
            'type':'teacher'
        }
    },
    'teacherGuidance':{
        'page':[
            {'page_id':102,'page_link':'/assess-class','page_text':'รายการประเมิน','page_icon':'list layout'},
            {'page_id':103,'page_link':'/assess-result','page_text':'ผลการประเมิน','page_icon':'bar chart'},
            {'page_id':104,'page_link':'/assess-send','page_text':'จัดการแบบประเมิน','page_icon':'file'}
        ],
        'user':{
            'type':'teacher'
        }
    }
};

router.route('/')
    .get((req, res) => {
        let role = req.user.role;
        let responseData = {};
        if (role === 'student') {
            responseData = roleList.student;
        } else if (role === 'parent') {
            responseData = roleList.parent;
        }  else if (role === 'teacher') {
            responseData = roleList.teacherGuidance;
        }
        return res.json(responseData);
    });

router.route('/check')
    .post(validate(paramValidation.checkRole),
        (req, res) => {
            let link = req.body.link;
            let role = req.user.role;
            let haveRole = false;
            if (role === 'student') {
                roleList.student.page.map((value) => {
                    if (value.page_link === link) {
                        haveRole = true;
                    }
                })
            } else if (role === 'parent') {
                roleList.parent.page.map((value) => {
                    if (value.page_link === link) {
                        haveRole = true;
                    }
                })
            } else if (role === 'teacher') {
                roleList.teacherGuidance.page.map((value) => {
                    if (value.page_link === link) {
                        haveRole = true;
                    }
                })
            }
            return res.json({
                'result': haveRole
            });
        }
    );

export default router;
