import express from 'express';
import expressJwt from 'express-jwt';
import httpStatus from 'http-status';
import mysql from 'mysql';
import async from 'async';
import validate from 'express-validation';
import paramValidation from '../../config/param-validation';
import config from '../../config/env';
import APIError from '../helpers/APIError';

const connection = mysql.createPool(config.db);
const router = express.Router(); // eslint-disable-line new-cap

router.use(expressJwt({
    secret: config.jwtSecret,
}));

router.route('/')
    .get((req, res, next) => {
        // student
        let userId = req.user.user_id;
        let role = req.user.role;
        var studentIdList = null;
        let q1 = (callback) => {
            if (role === 'student') {
                studentIdList = [userId];
                callback();
            } else if (role === 'parent') {
                let sql = `SELECT student_parent_mapping.student_id FROM `+config.dbSchool+`.student_parent_mapping WHERE parent_id = ?;`
                connection.query(sql , [userId], function(err, data) {
                    if (err) {
                        const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                        return next(err);
                    } else {
                        studentIdList = data.map(function(value) { return value['student_id']; });
                        callback();
                    }
                });
            } else if (role === 'teacher') {
                let semesterId = req.query.semester_id;
                let sql = `SELECT student_class.student_id FROM `+config.dbSchool+`.teacher_class
                INNER JOIN `+config.dbSchool+`.student_class
                ON student_class.classroom_id = teacher_class.classroom_id
                WHERE teacher_class.teacher_id = ?
                AND teacher_class.semester_id = ?`
                connection.query(sql , [userId, semesterId], function(err, data) {
                    if (err) {
                        const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                        return next(err);
                    } else if (data.length === 0){
                        studentIdList = null;
                        callback(err,studentIdList);
                    } else {
                        studentIdList = data.map(function(value) { return value['student_id']; });
                        callback(err,studentIdList);
                    }
                });
            }
        }
        let q2 = (callback) => {
            let sql = `SELECT assess.assess_id,
            assess.assess_start,
            assess.assess_finish,
            assess_template.template_name,
            assess.student_id,
            assess.student_class_id
            FROM `+config.dbAssess+`.assess
            INNER JOIN `+config.dbAssess+`.assess_template
            ON assess_template.template_id = assess.template_id
            WHERE assess.assess_for = ?
            AND assess.student_id IN (?)
            AND assess.delete_at IS NULL`;
            let responseData = {};
            connection.query(sql, [role,studentIdList], function(err, data) {
                if (err) {
                    const err = new APIError('Database Error 1', httpStatus.INTERNAL_SERVER_ERROR, true);
                    return next(err);
                } else {
                    responseData = data;
                    async.map(responseData, function(row,callback){
                        let student_id = row.student_id;
                        let student_class_id = row.student_class_id;
                        let sql = `SELECT
                        case student.title
                            when 1 then 'ด.ช.'
                            when 2 then 'ด.ญ.'
                            when 3 then 'นาย'
                            when 4 then 'น.ส.'
                            when 5 then 'นาง'
                        end as title,
                        student.firstname,
                        student.lastname,
                        academy_class.name AS class,
                        academy_class.short_name AS class_shortname,
                        academy_classroom.room_id AS room
                        FROM `+config.dbSchool+`.student
                        INNER JOIN `+config.dbSchool+`.student_class
                        ON student.id = student_class.student_id
                        INNER JOIN `+config.dbSchool+`.academy_classroom
                        ON academy_classroom.id = student_class.classroom_id
                        INNER JOIN `+config.dbSchool+`.academy_class
                        ON academy_class.id = academy_classroom.class_id
                        INNER JOIN `+config.dbSchool+`.semester
                        ON semester.id = student_class.semester_id
                        WHERE student.id = ?
                        AND student_class.id = ?`;
                        connection.query(sql, [student_id, student_class_id], function(err, data) {
                            if (err) {
                                const err = new APIError('Database Error 2', httpStatus.INTERNAL_SERVER_ERROR, true);
                                return next(err);
                            } else if (data.length === 0){
                                callback(err, data);
                            } else {
                                callback(err, data[0]);
                            }
                        });
                    }, function(err, results) {
                        responseData.map((row,key) => {
                            row.student = results[key];
                        })
                        callback(err,responseData);
                    });
                }
            });
        }
        async.series([q1, q2], function(err, responseData) {
            res.json(responseData[1]);
        });
    });

router.route('/class')
    .get((req, res, next) => {
        let role = req.user.role;
        let userId = req.user.user_id;
        if (role !== 'teacher') {
            // check role
            const err = new APIError('Not Found', httpStatus.NOT_FOUND);
            return next(err);
        }
        var classRoomList = null;
        let semesterId = req.query.semester_id;
        let q1 = (callback) => {
            let sql = `SELECT classroom_id FROM `+config.dbSchool+`.teacher_class
            WHERE teacher_class.teacher_id = ?
            AND teacher_class.semester_id = ?`
            connection.query(sql , [userId, semesterId], function(err, data) {
                if (err) {
                    const err = new APIError('Database Error 1', httpStatus.INTERNAL_SERVER_ERROR, true);
                    return next(err);
                } else if (data.length === 0){
                    classRoomList = null;
                    callback(err,classRoomList);
                } else {
                    classRoomList = data.map(function(value) { return value['classroom_id']; });
                    callback(err,classRoomList);
                }
            });
        }
        let q2 = (callback) => {
            let sql = `SELECT
                classroom_id,
                student_class.total,
                semester_id,
                class_id,
                semester.name AS semester_name,
                room_id As room,
                academy_class.name AS class_name,
                academy_class.short_name AS class_shortname,
                student_class.assess_start,
                student_class.total_finish,
                student_class.template_name
                FROM (
                    SELECT COUNT(classroom_id) AS total,
                    classroom_id,
                    semester_id,
                    assess_template.template_name,
                    MAX(assess_start) AS assess_start,
                    MIN(student.code) AS first_student,
                    COUNT(assess_finish) AS total_finish
                    FROM `+config.dbAssess+`.assess
                    INNER JOIN `+config.dbAssess+`.assess_template
                    ON assess.template_id = assess_template.template_id
                    INNER JOIN `+config.dbSchool+`.student_class
                    ON student_class.id = assess.student_class_id
                    INNER JOIN `+config.dbSchool+`.student
                    ON student.id = student_class.student_id
                    WHERE assess_for = ?
                    AND student_class.classroom_id IN (?)
                    AND semester_id = ?
                    GROUP BY classroom_id,semester_id,template_name
                ) AS student_class
                INNER JOIN `+config.dbSchool+`.academy_classroom
                ON academy_classroom.id = student_class.classroom_id
                INNER JOIN `+config.dbSchool+`.academy_class
                ON academy_class.id = academy_classroom.class_id
                INNER JOIN `+config.dbSchool+`.semester
                ON semester.id = student_class.semester_id`;
            connection.query(sql , [role, classRoomList, semesterId], function(err, data) {
                if (err) {
                    console.log("----",JSON.stringify(err))
                    const err = new APIError('Database Error 2', httpStatus.INTERNAL_SERVER_ERROR, true);
                    return next(err);
                } else {
                    callback(err,data);
                }
            });
        }
        async.series([q1, q2], function(err, responseData) {
            res.json(responseData[1]);
        });
    });

router.route('/class/:id')
    .get((req, res, next) => {
        let role = req.user.role;
        let userId = req.user.user_id;
        let classroomId = req.params.id;
        if (role !== 'teacher') {
            // check role
            const err = new APIError('Not Found', httpStatus.NOT_FOUND);
            return next(err);
        }
        let semesterId = req.query.semester_id;
        let q1 = (callback) => {
            let sql = `SELECT student.id AS student_id,student.code,assess_id FROM `+config.dbSchool+`.student
            INNER JOIN `+config.dbSchool+`.student_class
            ON student_class.student_id = student.id
            INNER JOIN `+config.dbAssess+`.assess
            ON assess.student_id = student.id
            WHERE student_class.classroom_id IN (
                SELECT classroom_id FROM `+config.dbSchool+`.teacher_class
                WHERE teacher_class.teacher_id = ?
                AND teacher_class.semester_id = ?
                AND teacher_class.classroom_id = ?
            )
            AND student_class.semester_id = ?
            AND assess.assess_for = ?
            ORDER BY student.code`
            connection.query(sql , [userId, semesterId, classroomId, semesterId, role], function(err, data) {
                if (err) {
                    const err = new APIError('Database Error 1', httpStatus.INTERNAL_SERVER_ERROR, true);
                    return next(err);
                } else if (data.length === 0){
                    callback(err,data);
                } else {
                    callback(err,data);
                }
            });
        }
        async.series([q1], function(err, responseData) {
            res.json(responseData[0]);
        });
    });

router.route('/class/:id/first')
    .get((req, res, next) => {
        let role = req.user.role;
        let userId = req.user.user_id;
        let classroomId = req.params.id;
        if (role !== 'teacher') {
            // check role
            const err = new APIError('Not Found', httpStatus.NOT_FOUND);
            return next(err);
        }
        let semesterId = req.query.semester_id;
        let q1 = (callback) => {
            let sql = `SELECT student.id AS student_id,student.code,assess_id FROM `+config.dbSchool+`.student
            INNER JOIN `+config.dbSchool+`.student_class
            ON student_class.student_id = student.id
            INNER JOIN `+config.dbAssess+`.assess
            ON assess.student_id = student.id
            WHERE student_class.classroom_id IN (
                SELECT classroom_id FROM `+config.dbSchool+`.teacher_class
                WHERE teacher_class.teacher_id = ?
                AND teacher_class.semester_id = ?
                AND teacher_class.classroom_id = ?
            )
            AND student_class.semester_id = ?
            AND assess.assess_for = ?
            ORDER BY student.code
            LIMIT 1`
            connection.query(sql , [userId, semesterId, classroomId, semesterId, role], function(err, data) {
                if (err) {
                    const err = new APIError('Database Error 1', httpStatus.INTERNAL_SERVER_ERROR, true);
                    return next(err);
                } else if (data.length === 0){
                    callback(err,data);
                } else {
                    callback(err,data[0]);
                }
            });
        }
        async.series([q1], function(err, responseData) {
            res.json(responseData[0]);
        });
    });

router.route('/:id/student')
    .get((req, res, next) => {
        let userId = req.user.user_id;
        let assessId = req.params.id;
        let role = req.user.role;
        var studentIdList = [];
        let q1 = (callback) => {
            if (role === 'student') {
                studentIdList = [userId];
                callback();
            } else if (role === 'parent') {
                let sql = `SELECT student_id FROM `+config.dbSchool+`.student_parent_mapping WHERE parent_id = ?;`
                connection.query(sql , [userId], function(err, data) {
                    if (err) {
                        const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                        return next(err);
                    } else if (data.length === 0){
                        studentIdList = null;
                        callback(err,studentIdList);
                    } else {
                        studentIdList = data.map(function(value) { return value['student_id']; });
                        callback();
                    }
                });
            } else if (role === 'teacher') {
                let semesterId = req.query.semester_id;
                let sql = `SELECT student_class.student_id FROM `+config.dbSchool+`.teacher_class
                INNER JOIN student_class
                ON student_class.classroom_id = teacher_class.classroom_id
                WHERE teacher_class.teacher_id = ?
                AND teacher_class.semester_id = ?`
                connection.query(sql , [userId, semesterId], function(err, data) {
                    if (err) {
                        const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                        return next(err);
                    } else if (data.length === 0){
                        studentIdList = null;
                        callback(err,studentIdList);
                    } else {
                        studentIdList = data.map(function(value) { return value['student_id']; });
                        callback();
                    }
                });
            }
        }
        let q2 = (callback) => {
            let sql = `SELECT
            student.id,
            student.code,
            case student.title
                when 1 then 'ด.ช.'
                when 2 then 'ด.ญ.'
                when 3 then 'นาย'
                when 4 then 'น.ส.'
                when 5 then 'นาง'
            end AS title,
            student.firstname,
            student.lastname,
            academy_class.name AS class,
            academy_class.short_name AS class_shortname,
            academy_classroom.room_id AS room,
            semester.name AS semaster
            FROM `+config.dbAssess+`.assess

            INNER JOIN `+config.dbSchool+`.student
            ON student.id = assess.student_id
            INNER JOIN `+config.dbSchool+`.student_class
            ON student_class.id = assess.student_class_id
            INNER JOIN `+config.dbSchool+`.semester
            ON semester.id = student_class.semester_id
            INNER JOIN `+config.dbSchool+`.academy_classroom
            ON academy_classroom.id = student_class.classroom_id
            INNER JOIN `+config.dbSchool+`.academy_class
            ON academy_class.id = academy_classroom.class_id
            WHERE assess.assess_id = ?
            AND assess.assess_for = ?
            AND assess.student_id IN (?)`
            connection.query(sql, [assessId, role, studentIdList], function(err, data) {
                if (err) {
                    const err = new APIError('Database Error 1', httpStatus.INTERNAL_SERVER_ERROR, true);
                    return next(err);
                } else if (data.length === 0){
                    const err = new APIError('Unauthorized', httpStatus.UNAUTHORIZED, true);
                    return next(err);
                } else {
                    callback(err,data[0]);
                }
            });
        }
        async.series([q1, q2], function(err, responseData) {
            res.json(responseData[1]);
        });
    });

router.route('/:id/question')
    .get((req, res, next) => {
        let userId = req.user.user_id;
        let assessId = req.params.id;
        let role = req.user.role;
        var studentIdList = [];
        let q1 = (callback) => {
            if (role === 'student') {
                studentIdList = [userId];
                callback();
            } else if (role === 'parent') {
                let sql = `SELECT student_id FROM `+config.dbSchool+`.student_parent_mapping WHERE parent_id = ?;`
                connection.query(sql , [userId], function(err, data) {
                    if (err) {
                        const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                        return next(err);
                    } else if (data.length === 0){
                        studentIdList = null;
                        callback(err,studentIdList);
                    } else {
                        studentIdList = data.map(function(value) { return value['student_id']; });
                        callback();
                    }
                });
            } else if (role === 'teacher') {
                let semesterId = req.query.semester_id;
                let sql = `SELECT student_class.student_id FROM `+config.dbSchool+`.teacher_class
                INNER JOIN student_class
                ON student_class.classroom_id = teacher_class.classroom_id
                WHERE teacher_class.teacher_id = ?
                AND teacher_class.semester_id = ?`
                connection.query(sql , [userId, semesterId], function(err, data) {
                    if (err) {
                        const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                        return next(err);
                    } else if (data.length === 0){
                        studentIdList = null;
                        callback(err,studentIdList);
                    } else {
                        studentIdList = data.map(function(value) { return value['student_id']; });
                        callback();
                    }
                });
            }
        }
        let q2 = (callback) => {
            let sql = `SELECT assess_question.*,
            assess_user_answer.assess_answer_id,
            assess_user_answer.answer_id
            FROM `+config.dbAssess+`.assess
            INNER JOIN `+config.dbAssess+`.assess_template
            ON assess.template_id = assess_template.template_id
            INNER JOIN `+config.dbAssess+`.assess_question
            ON assess_question.template_id = assess_template.template_id
            LEFT JOIN `+config.dbAssess+`.assess_user_answer
            ON assess.assess_id = assess_user_answer.assess_id AND assess_question.question_id = assess_user_answer.question_id
            WHERE assess.assess_id = ?
            AND assess.student_id IN (?)`
            connection.query(sql, [assessId, studentIdList], function(err, data) {
                if (err) {
                    const err = new APIError('Database Error 1', httpStatus.INTERNAL_SERVER_ERROR, true);
                    return next(err);
                } else if (data.length === 0){
                    const err = new APIError('Unauthorized', httpStatus.UNAUTHORIZED, true);
                    return next(err);
                } else {
                    let responseData = data;
                    async.map(responseData, function(row,callback){
                        let questionId = row.question_id;
                        let sql = `SELECT *
                        FROM `+config.dbAssess+`.assess_answer
                        WHERE question_id = ?`;
                        connection.query(sql, [questionId], function(err, data) {
                            if (err) {
                                const err = new APIError('Database Error 2', httpStatus.INTERNAL_SERVER_ERROR, true);
                                return next(err);
                            } else if (data.length === 0){
                                callback(err, data);
                            } else {
                                callback(err, data);
                            }
                        });
                    }, function(err, results) {
                        responseData.map((row,key) => {
                            row.answer = results[key];
                        })
                        callback(err,responseData);
                    });
                }
            });
        }
        async.series([q1, q2], function(err, responseData) {
            res.json(responseData[1]);
        });
    });

router.route('/:id/answer')
    .post(validate(paramValidation.checkAnswer),(req, res, next) => {
        let assessId = req.params.id;
        let userId = req.user.user_id;
        let questionId = req.body.question_id;
        let answerId = req.body.answer_id;
        let assessAnswerId;
        let sql = `SELECT assess_answer_id FROM `+config.dbAssess+`.assess_user_answer WHERE question_id = ? AND assess_id = ?`;
        let q1 = (callback) => {
            connection.query(sql, [questionId, assessId], function(err, data) {
                if (err) {
                    const err = new APIError('Database Error 1', httpStatus.INTERNAL_SERVER_ERROR, true);
                    return next(err);
                } else if (data.length === 0){
                    sql = `INSERT INTO `+config.dbAssess+`.assess_user_answer(assess_id,question_id,answer_id,user_id) VALUES (?,?,?,?)`;
                    connection.query(sql, [assessId,questionId,answerId,userId], function(err, data) {
                        if (err) {
                            const err = new APIError('Database Error 2', httpStatus.INTERNAL_SERVER_ERROR, true);
                            return next(err);
                        } else {
                            callback(err,data);
                        }
                    });
                } else {
                    assessAnswerId = data[0].assess_answer_id;
                    sql = `UPDATE `+config.dbAssess+`.assess_user_answer
                    SET answer_id = ?,user_id = ?,update_at = CURRENT_TIMESTAMP
                    WHERE assess_user_answer.assess_answer_id = ?`
                    connection.query(sql, [answerId, userId, assessAnswerId], function(err, data) {
                        if (err) {
                            const err = new APIError('Database Error 3', httpStatus.INTERNAL_SERVER_ERROR, true);
                            return next(err);
                        } else {
                            callback(err,data);
                        }
                    });
                }
            });
        }
        let q2 = (callback) => {
            sql = `SELECT COUNT(assess_question.question_id) AS question_total,COUNT(assess_user_answer.assess_answer_id) AS answer_total FROM schoolassess.assess
                INNER JOIN schoolassess.assess_template
                ON assess.template_id = assess_template.template_id
                INNER JOIN schoolassess.assess_question
                ON assess_question.template_id = assess_template.template_id
                LEFT JOIN schoolassess.assess_user_answer
                ON assess.assess_id = assess_user_answer.assess_id AND assess_question.question_id = assess_user_answer.question_id
                WHERE assess.assess_id = ?`;
            connection.query(sql, [assessId], function(err, data) {
                if (err) {
                    const err = new APIError('Database Error 1', httpStatus.INTERNAL_SERVER_ERROR, true);
                    return next(err);
                } else if (data.length === 0){
                    callback(err,data);
                } else {
                    if (data[0].question_total === data[0].answer_total) {
                        sql = `UPDATE schoolassess.assess SET assess_finish = CURRENT_TIMESTAMP
                        WHERE assess_id = ?`;
                        connection.query(sql, [assessId], function(err, data2) {
                            if (err) {
                                const err = new APIError('Database Error 2', httpStatus.INTERNAL_SERVER_ERROR, true);
                                return next(err);
                            } else {
                                callback(err,data2);
                            }
                        });
                    } else {
                        callback(err,data);
                    }
                }
            });
        }
        async.series([q1, q2], function(err, responseData) {
            res.json(responseData[0]);
        });

    });

export default router;
