import express from 'express';
import jwt from 'jsonwebtoken';
import httpStatus from 'http-status';
import validate from 'express-validation';
import expressJwt from 'express-jwt';
import mysql from 'mysql';
import paramValidation from '../../config/param-validation';
import config from '../../config/env';
import APIError from '../helpers/APIError';

const connection = mysql.createPool(config.db);
const router = express.Router();    // eslint-disable-line new-cap


router.route('/login')
    .get((req,res) => {
        res.send("ok");
    })
    .post(validate(paramValidation.login), (req, res, next) => {
        let sql = `SELECT * FROM `+config.dbAssess+`.example_user WHERE user = ? AND password = ?`;
        connection.query(sql, [req.body.username, req.body.password], function(err, data) {
            if (err) {
                const err = new APIError('Database Error', httpStatus.INTERNAL_SERVER_ERROR, true);
                return next(err);
            } else if (data.length === 0) {
                const err = new APIError('Authentication error', httpStatus.UNAUTHORIZED, true);
                return next(err);
            } else {
                const token = jwt.sign({
                    user_id: data[0].user_id,
                    school_id: data[0].school_id,
                    role: data[0].type
                }, config.jwtSecret);
                return res.json({
                    token,
                    role: data[0].type
                });
            }
        });
    });
/** GET /api/auth/random-number - Protected route,
 * needs token returned by the above as header. Authorization: Bearer {token} */

router.route('/random-number')
    .get(expressJwt({
        secret: config.jwtSecret,
    }), (req, res, next) => {
        if (req.user.role !== 'student') {
            // check role
            const err = new APIError('Not Found', httpStatus.NOT_FOUND);
            return next(err);
        }
        return res.json({
            user: req.user,
            num: Math.random() * 100,
        });
    });
export default router;
