module.exports = {
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true
        }
    },
    "rules": {
        "indent": [
            2,
            4, {
                "SwitchCase": 1
            }
        ],
        "no-unused-vars": [
            1
        ],
    }
};
