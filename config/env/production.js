export default {
    env: 'production',
    jwtSecret: '0a6b944d-d2fb-46fc-a85e-0295c986cd9f',
    db: {
        user: 'root',
        password: '',
        host: 'localhost',
        port: 3306,
        multipleStatements: true,
    },
    dbAssess: 'schoolassess',
    dbSchool: 'schoolcare',
    host: 'localhost',
    port: 5000
};
