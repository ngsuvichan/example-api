import Joi from 'joi';

export default {
    // POST /api/auth/login
    login: {
        body: {
            username: Joi.string().required(),
            password: Joi.string().required()
        }
    },
    // POST /api/privilege/check
    checkRole: {
        body: {
            link: Joi.string().required()
        },
    },

    checkAnswer: {
        body: {
            question_id: Joi.number().integer().required(),
            answer_id: Joi.number().integer().required()
        },
    },
    checkSendList: {
        query: {
            role: Joi.string().required(),
            semester_id: Joi.number().integer().required()
        },
    },
    checkSendAssess: {
        body: {
            semester_id: Joi.number().integer().required(),
            role: Joi.string().required(),
        },
    },
    checkTemplateFilter: {
        query: {
            role: Joi.string().required()
        },
    },

};
