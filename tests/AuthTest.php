<?php

require 'vendor/autoload.php';
use GuzzleHttp\Client;


class AuthTest extends PHPUnit_Framework_TestCase
{
    protected $apiBase = 'http://sc-api/v1/';
    public $client;
    public $token;
    public $role;

    protected function setUp()
    {
        $this->client = new Client([
            'base_uri' => $this->apiBase,
        ]);
    }

    public function login()
    {
        $data     = ['username' => 'admin_nx', 'password' => 'adminnx'];
        $response = $this->client->post('auth', [
            'form_params' => $data]
        );

        $responseJson = json_decode($response->getBody());
        $this->token  = $responseJson->token;
        $this->role   = $responseJson->role;
        return $response;
    }

    public function testAuth()
    {
        $response = $this->login();
        $this->assertEquals(200, $response->getStatusCode());
        $responseJson = json_decode($response->getBody());
        $this->token  = $responseJson->token;
        $this->role   = $responseJson->role;
        $this->assertEquals('teacher', $this->role);
    }

    public function testValidateToken()
    {
    		$this->login();
        $response = $this->client->request('GET', 'template', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->token,
            ],
            'debug'   => false,
        ]);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
