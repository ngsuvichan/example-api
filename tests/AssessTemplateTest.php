<?php

require 'vendor/autoload.php';
require_once 'AuthTest.php';

class AssessTemplateTest extends PHPUnit_Framework_TestCase
{
    protected $client;
    protected $auth;

    protected function setUp()
    {
        $auth = new AuthTest();
        $auth->setUp();
        $auth->login();
        $this->auth = $auth;
    }

    public function testGetTemplateList()
    {
        $this->assertEquals('teacher', $this->auth->role);

        $response = $this->auth->client->request('GET', 'template', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->auth->token,
            ],
            'debug'   => false,
        ]);
        $data = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('template_id', $data[0]);
        $this->assertArrayHasKey('template_name', $data[0]);
        $this->assertArrayHasKey('template_for', $data[0]);
        $this->assertArrayHasKey('create_at', $data[0]);
        $this->assertArrayHasKey('update_at', $data[0]);
        $this->assertArrayHasKey('delete_at', $data[0]);
    }

    public function testGetTemplateById()
    {
        $this->assertEquals('teacher', $this->auth->role);

        $response = $this->auth->client->request('GET', 'template/1', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->auth->token,
            ],
            'debug'   => false,
        ]);
        $data = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('template_id', $data);
        $this->assertArrayHasKey('template_name', $data);
        $this->assertArrayHasKey('template_for', $data);
        $this->assertArrayHasKey('create_at', $data);
        $this->assertArrayHasKey('update_at', $data);
        $this->assertArrayHasKey('delete_at', $data);
    }

    public function testGetTemplateByRole()
    {
        $this->assertEquals('teacher', $this->auth->role);

        $response = $this->auth->client->request('GET', 'template', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->auth->token,
            ],
            'query'   => [
                'role' => 'student',
            ],
            'debug'   => false,
        ]);
        $data = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertArrayHasKey('template_id', $data[0]);
        $this->assertArrayHasKey('template_name', $data[0]);
        $this->assertArrayHasKey('template_for', $data[0]);
        $this->assertArrayHasKey('create_at', $data[0]);
        $this->assertArrayHasKey('update_at', $data[0]);
        $this->assertArrayHasKey('delete_at', $data[0]);

        $this->assertEquals('student', $data[0]['template_for']);

    }

    public function testGetTemplateSendlist()
    {
        $this->assertEquals('teacher', $this->auth->role);

        $response = $this->auth->client->request('GET', 'template/1/send-list', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->auth->token,
            ],
            'query'   => [
                'role'        => 'teacher',
                'semester_id' => 147,
            ],
            'debug'   => false,
        ]);
        $data = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());

        if (is_array($data) && !empty($data)) {
            $this->assertArrayHasKey('classroom_id', $data[0]);
            $this->assertArrayHasKey('class_id', $data[0]);
            $this->assertArrayHasKey('class_name', $data[0]);
            $this->assertArrayHasKey('class_shortname', $data[0]);
            $this->assertArrayHasKey('room', $data[0]);
            $this->assertArrayHasKey('is_notsend', $data[0]);
            $this->assertArrayHasKey('is_send', $data[0]);
            $this->assertArrayHasKey('total', $data[0]);

            $this->assertEquals($data[0]['total'], $data[0]['is_notsend'] + $data[0]['is_send']);

        }

    }

    public function testGetTemplateSendlistWithClassroomId()
    {
        $this->assertEquals('teacher', $this->auth->role);

        $response = $this->auth->client->request('GET', 'template/1/send-list/2943', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->auth->token,
            ],
            'query'   => [
                'role'        => 'teacher',
                'semester_id' => 147,
            ],
            'debug'   => false,
        ]);
        $data = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());

        if (is_array($data) && !empty($data)) {
            $this->assertArrayHasKey('classroom_id', $data[0]);
            $this->assertArrayHasKey('class_id', $data[0]);
            $this->assertArrayHasKey('class_name', $data[0]);
            $this->assertArrayHasKey('class_shortname', $data[0]);
            $this->assertArrayHasKey('room', $data[0]);
            $this->assertArrayHasKey('is_notsend', $data[0]);
            $this->assertArrayHasKey('is_send', $data[0]);
            $this->assertArrayHasKey('total', $data[0]);

            $this->assertEquals($data[0]['total'], $data[0]['is_notsend'] + $data[0]['is_send']);

        }

    }

    public function testSendTemplateByClassroom()
    {
        $this->assertEquals('teacher', $this->auth->role);
        //delete 2942 first
        $response = $this->auth->client->request('POST', 'template/1/send/2942', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->auth->token,
            ],
            'form_params'   => [
                'role'        => 'teacher',
                'semester_id' => 147,
            ],
            'debug'   => false,
        ]);
        $data = json_decode($response->getBody(), true);
        if (is_array($data) && !empty($data)) {
            $this->assertArrayHasKey('classroom_id', $data[0]);
            $this->assertArrayHasKey('class_id', $data[0]);
            $this->assertArrayHasKey('class_name', $data[0]);
            $this->assertArrayHasKey('class_shortname', $data[0]);
            $this->assertArrayHasKey('room', $data[0]);
            $this->assertArrayHasKey('is_notsend', $data[0]);
            $this->assertArrayHasKey('is_send', $data[0]);
            $this->assertArrayHasKey('total', $data[0]);

            $this->assertEquals($data[0]['total'], $data[0]['is_notsend'] + $data[0]['is_send']);

        }

    }

    public function testRemoveTemplateByClassroom()
    {
        $this->assertEquals('teacher', $this->auth->role);
        //delete 2942 first
        $response = $this->auth->client->request('DELETE', 'template/1/send/2942', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->auth->token,
            ],
            'form_params'   => [
                'role'        => 'teacher',
                'semester_id' => 147,
            ],
            'debug'   => false,
        ]);
        $data = json_decode($response->getBody(), true);
        if (is_array($data) && !empty($data)) {
            $this->assertArrayHasKey('classroom_id', $data[0]);
            $this->assertArrayHasKey('class_id', $data[0]);
            $this->assertArrayHasKey('class_name', $data[0]);
            $this->assertArrayHasKey('class_shortname', $data[0]);
            $this->assertArrayHasKey('room', $data[0]);
            $this->assertArrayHasKey('is_notsend', $data[0]);
            $this->assertArrayHasKey('is_send', $data[0]);
            $this->assertArrayHasKey('total', $data[0]);

            $this->assertEquals($data[0]['total'], $data[0]['is_notsend'] + $data[0]['is_send']);

        }

    }
}
