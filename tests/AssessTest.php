<?php

require 'vendor/autoload.php';
require_once 'AuthTest.php';

class AssessTest extends PHPUnit_Framework_TestCase
{
    protected $client;
    protected $auth;

    protected function setUp()
    {
        $auth = new AuthTest();
        $auth->setUp();
        $auth->login();
        $this->auth = $auth;
    }

    public function testGetAssessList()
    {
        $this->assertEquals('teacher', $this->auth->role);

        $response = $this->auth->client->request('GET', 'assess', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->auth->token,
            ],
            'query'   => [
                'semester_id' => 147,
            ],
            'debug'   => false,
        ]);
        $data = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
        $assessRow = $data[0];

        if (is_array($assessRow) && !empty($assessRow)) {
            $this->assertArrayHasKey('title', $assessRow[0]);
            $this->assertArrayHasKey('firstname', $assessRow[0]);
            $this->assertArrayHasKey('lastname', $assessRow[0]);
            $this->assertArrayHasKey('class', $assessRow[0]);
            $this->assertArrayHasKey('class_shortname', $assessRow[0]);
            $this->assertArrayHasKey('room', $assessRow[0]);
        }
    }

    public function testGetAssessListByClass()
    {
        $this->assertEquals('teacher', $this->auth->role);

        $response = $this->auth->client->request('GET', 'assess/class', [
            'headers' => [
                'Authorization' => 'Bearer ' . $this->auth->token,
            ],
            'debug'   => false,
        ]);
        $data = json_decode($response->getBody(), true);
        $this->assertEquals(200, $response->getStatusCode());
    }
}
