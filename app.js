import config from './config/env';
import app from './config/express';
import debug from 'debug';

// listen on port config.port
app.listen(config.port, () => {
    debug(`server started on port ${config.port} (${config.env})`);
    console.log(`server started on host http://${config.host}:${config.port} (${config.env})`);
});

export default app;
